@extends('layouts.app')

@section('content')

    @if (auth()->check())
        @if (auth()->user()->hasRole('admin'))
            <h1>Displaying Admin content</h1>
        @elseif (auth()->user()->hasRole('moderator'))
            <h1>Displaying moderator content</h1>
        @elseif(auth()->user()->hasRole('user'))
            <h1>User Content</h1>
        @endif
    @else
        <h1>Displaying guest content</h1>
    @endif

@endsection