<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>
        Foundation for Sites
    </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{{asset('dist/css/foundation.css')}}"/>
    <link rel="stylesheet" href="{{('dist/css/app.css')}}"/>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">


</head>
<body>
<div  class="top-bar">
    <div style="color:white" class="top-bar-left">
        <h4 class="brand-title">
            <a href="{{route('main')}}">
                <i class="fa fa-paint-brush fa-lg" aria-hidden="true">
                </i>
                Yuri Moro Prints
            </a>
        </h4>
    </div>
    <div class="top-bar-right">
        <ol class="menu">
            @if (auth()->check())
                @if (auth()->user()->hasRole('admin'))
                    <li>
                        <a href="{{url('/logout')}}">
                            LOGOUT
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.index') }}">
                            DASHBOARD
                        </a>
                    </li>
                @elseif (auth()->user()->hasRole('user'))
                    <li>
                        <a href="{{url('/logout')}}">
                            LOGOUT
                        </a>
                    </li>
                    <li>
                        <a href="">
                            PROFILE
                        </a>
                    </li>
                @endif
            @else
                <li>
                    <a href="{{ route('login') }}">
                        LOGIN
                    </a>
                </li>
            @endif

            <li>
                <a href="{{route('products')}}">
                    PRODUCTS
                </a>
            </li>
            <li>
                <a href="{{route('cart.index')}}">
                    <i class="fa fa-shopping-cart fa-2x" aria-hidden="true">
                    </i>
                    CART
                    <span class="alert badge">
                        {{Cart::count()}}
                    </span>
                </a>
            </li>
        </ol>
    </div>
</div>

@yield('content')

<footer class="footer">
    <div class="row full-width">
        <div class="col-sm-12">
            <i class="fi-laptop"></i>
            <p>Made by Andrii Hermaniuk</p>
        </div>
    </div>
</footer>

<script src="{{asset('dist/js/vendor/jquery.js')}}"></script>
<script src="{{asset('dist/js/app.js')}}"></script>
</body>
</html>