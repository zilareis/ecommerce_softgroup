@extends('layout.main')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="sidebar content-box">
                @if(!empty($categories))
                        <ul class="nav">
                            <h6>Categories</h6>
                            @forelse($categories as $category)
                            <li><a href="{{route('category', $category->id)}}">{{$category->title}}</a></li>
                            @empty
                                <h3>No items</h3>
                        </ul>
                    @endforelse
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                @forelse($products as $product)
                    <div class="small-3 columns">
                        <div class="item-wrapper">
                            <div class="img-wrapper">
                                <a class="button expanded add-to-cart" href="{{route('cart.edit', $product->id)}}">
                                    Add to Cart
                                </a>
                                <a href="#">
                                    <img src="{{url('images', $product->image)}}"/>
                                </a>
                            </div>
                            <a href="{{route('product', $product->id)}}">
                                <h3>
                                    {{$product->title}}
                                </h3>
                            </a>
                            <h5>
                                ${{$product->price}}
                            </h5>
                            <p>
                                {{$product->description}}
                            </p>
                        </div>
                    </div>
                @empty
                    <h3>No products</h3>
                @endforelse
            </div>
        </div>
    </div>
@endsection