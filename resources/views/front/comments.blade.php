@extends('layout.main')

@section('content')

    <section class="posts endless-pagination" data-next-page="{{ $posts->nextPageUrl() }}">
        @foreach($posts as $post)

            <div class="article">
                <h2>{{ $post->title }}</h2>
                {{ $post->content }}
            </div>

        @endforeach

    </section>
    <script>

        $(document).ready(function() {
            $(window).scroll(fetchPosts);

            function fetchPosts() {

                var page = $('.endless-pagination').data('next-page');

                if(page !== null) {

                    clearTimeout( $.data( this, "scrollCheck" ) );

                    $.data( this, "scrollCheck", setTimeout(function() {
                        var scroll_position_for_posts_load = $(window).height() + $(window).scrollTop() + 100;

                        if(scroll_position_for_posts_load >= $(document).height()) {
                            $.get(page, function(data){
                                $('.posts').append(data.posts);
                                $('.endless-pagination').data('next-page', data.next_page);
                            });
                        }
                    }, 350))

                }
            }


        })

    </script>
@stop