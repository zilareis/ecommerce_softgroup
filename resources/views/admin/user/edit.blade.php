@extends('admin.layout.admin')

@section('content')

    <h3>Edit user</h3>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            {!! Form::open(['route'=>['user.update', $user->id], 'method'=>'PUT', 'files'=>true]) !!}
                <div class="form-group">
                    {{ Form::label('role', 'Role:') }}
                    {{ Form::label('role', 'User') }}
                    {{ Form::radio('role', 'user' , true) }}
                    {{ Form::label('role', 'Admin') }}
                    {{ Form::radio('role', 'admin' , false) }}
                </div>

            {{ Form::submit('Edit', array('class' => 'btn btn-default'))}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection