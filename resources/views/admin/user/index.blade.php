@extends('admin.layout.admin')

@section('content')
    <h3>Users</h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <td>ID</td>
            <td>E-mail</td>
            <td>Role</td>
            <td colspan="2">Action</td>
        </tr>
        </thead>
        <tbody>
        @forelse($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->role}}</td>
                <td><a href="{{ route('user.edit',$user->id)}}" class="btn btn-primary">Edit</a></td>
                <td>
                    <form action="{{ route('user.destroy', $user->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @empty
            <h3>No users</h3>
        @endforelse
        </tbody>
    </table>
@endsection