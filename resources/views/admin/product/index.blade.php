@extends('admin.layout.admin')

@section('content')
    <h3>Products</h3>
    <a href="{{route('product.create')}}">Add Product</a>
    <table class="table table-striped">
        <thead>
        <tr>
            <td>ID</td>
            <td>Title</td>
            <td>Description</td>
            <td>Price</td>
            <td>Category</td>
            <td>Image</td>
            <td colspan="2">Action</td>
        </tr>
        </thead>
        <tbody>
        @forelse($products as $product)
            <tr>
                <td>{{$product->id}}</td>
                <td>{{$product->title}}</td>
                <td>{{$product->description}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->category->title}}</td>
                <td><img src="{{url('images', $product->image)}}"/></td>
                <td><a href="{{ route('product.edit',$product->id)}}" class="btn btn-primary">Edit</a></td>
                <td>
                    <form action="{{ route('product.destroy', $product->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @empty
            <h3>No products</h3>
        @endforelse
        </tbody>
    </table>
@endsection