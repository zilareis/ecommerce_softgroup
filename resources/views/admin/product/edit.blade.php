@extends('admin.layout.admin')

@section('content')

    <h3>Edit product</h3>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            {!! Form::open(['route'=>['product.update', $product->id], 'method'=>'PUT', 'files'=>true]) !!}
            <div class="form-group">
                {{ Form::label('title', 'Title') }}
                {{ Form::text('title', $product->title, array('class'=>'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('description', 'Description') }}
                {{ Form::text('description', $product->description, array('class'=>'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('price', 'Price') }}
                {{ Form::text('price', $product->price, array('class'=>'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('image', 'Image') }}
                {{ Form::file('image', array('class'=>'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('category_id', 'Category') }}
                {{ Form::select('category_id', $categories, null, ['class'=>'form-control']) }}
            </div>

            {{ Form::submit('Edit', array('class' => 'btn btn-default'))}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection