@extends('admin.layout.admin')

@section('content')

    <h3>Add category</h3>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            {!! Form::open(['route'=>'category.store', 'method'=>'post', 'files'=>true]) !!}
            <div class="form-group">
                {{ Form::label('title', 'Title') }}
                {{ Form::text('title', null, array('class'=>'form-control')) }}
            </div>

            {{ Form::submit('Create', array('class' => 'btn btn-default'))}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection