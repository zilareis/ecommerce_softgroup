@extends('admin.layout.admin')

@section('content')
    <h3>Categories</h3>
    <a href="{{route('category.create')}}">Add Category</a>
    <table class="table table-striped">
        <thead>
        <tr>
            <td>ID</td>
            <td>Title</td>
            <td colspan="2">Action</td>
        </tr>
        </thead>
        <tbody>
        @forelse($categories as $category)
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->title}}</td>
                <td><a href="{{ route('category.edit',$category->id)}}" class="btn btn-primary">Edit</a></td>
                <td>
                    <form action="{{ route('category.destroy', $category->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @empty
            <h3>No categories</h3>
        @endforelse
        </tbody>
    </table>
@endsection