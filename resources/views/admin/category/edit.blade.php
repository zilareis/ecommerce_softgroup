@extends('admin.layout.admin')

@section('content')

    <h3>Edit category</h3>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            {!! Form::open(['route'=>['category.update', $category->id], 'method'=>'PUT', 'files'=>true]) !!}
            <div class="form-group">
                {{ Form::label('title', 'Title') }}
                {{ Form::text('title', $category->title, array('class'=>'form-control')) }}
            </div>

            {{ Form::submit('Edit', array('class' => 'btn btn-default'))}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection