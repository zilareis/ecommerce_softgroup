<div class="col-md-2">
    <div class="sidebar content-box" style="display: block;">
        <ul class="nav">
            <li class="current"><a href="{{route('admin.index')}}"><i class="glyphicon glyphicon-home"></i>
                    Dashboard</a></li>
            <li><a href="{{route('product.index')}}">Products</a></li>
            <li><a href="{{route('category.index')}}">Categories</a></li>
            <li><a href="{{route('user.index')}}">Users</a></li>
            <li><a href="{{route('category.index')}}">Orders</a></li>
        </ul>
    </div>
</div>