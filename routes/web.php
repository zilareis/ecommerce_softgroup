<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/products', 'FrontController@products')->name('products');

Route::get('/category/{id}', 'FrontController@show')->name('category');

Route::get('/product/{id}', 'FrontController@product')->name('product');

Route::group(['prefix' => 'admin', 'middleware'=>'auth'], function() {
    Route::get('/', function () {
        return view('admin.index');
    })->name('admin.index');

    Route::resource('product', 'ProductsController');
    Route::resource('category', 'CategoriesController');
    Route::resource('user', 'UsersController');
});

Route::get('/', 'FrontController@index')->name('main');

Route::resource('/cart', 'CartController');

Route::group(['middleware' => ['web']], function () {

    Route::get('/comments/{id}', 'FrontController@showComments')->name('comments');

});
