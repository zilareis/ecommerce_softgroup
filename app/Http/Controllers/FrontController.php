<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

use App\Post;
use App\Http\Requests;


class FrontController extends Controller
{
    public function index()
    {
        $products=Product::all();
        return view('front.home', compact('items'));
    }

    public function products()
    {
        $categories=Category::all();
        $products=Product::all();
        return view('front.products', compact('products', 'categories'));
    }

    public function product($id)
    {
        $product=Product::find($id);
        return view('front.product', compact('product'));
    }

    public function show($id)
    {
        $products=Category::find($id)->products;

        $categories=Category::all();

        return view('front.products',compact(['categories','products']));
    }

    protected $posts_per_page = 10;

    public function showComments(Request $request, $id) {

        $posts = Post::paginate($this->posts_per_page);

        if($request->ajax()) {
            return [
                'posts' => view('front.ajax.index')->with(compact('posts'))->render(),
                'next_page' => $posts->nextPageUrl()
            ];
        }

        return view('front.comments')->with(compact('posts'));

    }

    public function fetchNextPostsSet($page) {



    }

}